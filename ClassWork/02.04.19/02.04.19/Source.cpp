#include <iostream>
#include <string>

#define DAYS 365
#define SPACE "\t"
//----------------------------------------------------
#define SUM(a, b)((a)+(b))
#define DEBUG	// for turning off DEBUG just commit this line - " #define DEBUG "
#define PROD 20

union Test
{
	short a;
	int b;
	double c;
};



using namespace std;


int main()
{
	cout << DAYS << endl;
	cout << "Some Text" << SPACE << "Another text" << endl;
	cout << SUM(5, 10) << endl;
	cout << "---------------------------" << endl;

#ifdef DEBUG
	cout << "Start loop" << endl;	//here cout for developer(check something)
#endif
	for (int i = 0; i < 5; i++)
	{
		cout << i << endl;
	}
#ifdef DEBUG
	cout << "Ent loop" << endl;		//here cout for developer(check something)
#endif
	cout << "---------------------------" << endl;

#ifdef DEBUG
	cout << "Start loop" << endl;
#else
	cout << "Loop havent started" << endl;
#endif
	cout << "---------------------------" << endl;

#ifndef DEBUG
	cout << "Start loop" << endl;
#else
	cout << "Loop havent started" << endl;
#endif
	cout << "---------------------------" << endl;

#if PROD > 40
		cout << " PROD < 40 " << endl;
#elif PROD > 10;
	cout << "PROD > 10" << endl;
#else
	cout << "Error" << endl;
#endif
	cout << "---------------------------" << endl;

	Test un;
	un.a = 48;
	un.b = 122;
	un.c = 491;




	system("pause");
	return 0;
}