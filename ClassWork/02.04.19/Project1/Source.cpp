#include <iostream>
#include <string>
#define MAX(a, b, c)( (  ((a) > (b)) && ((a) > (c))? (a): (a) ) || ( ((b) > (a)) && ((b) > (c))? (b): (b)) || ( ((c) > (a)) && ((c) > (b))? (c): (c)))

// (if ((a) > (b) && (a) > (c) cout << a;) if ((b) > (a) && (b) > (c) cout << b;) if( ((c) > (a) && (c) > (b) cout << c;)))

using namespace std;

int main()
{
	cout << MAX(3, 5, 7) << endl;
	system("pause");
	return 0;
}