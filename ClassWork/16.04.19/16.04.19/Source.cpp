#include <iostream>;
#include <ctime>;
#include <string>;
#include <windows.h>

using namespace std;

//struct Person
//{
//	string name;
//	unsigned int age : 8;
//};

struct Time
{
	unsigned int hour : 6;
	unsigned int minute : 7;
	unsigned int second : 7;
	void Show()
	{
		cout << "\nHour: " << hour;
		cout << "\nMinute: " << minute;
		cout << "\nSecond: " << second << endl;
	}
};

int main()
{
	/*Person person;
	person.name = "Bill";
	person.age = 256;
	cout << "Name: " << person.name << "\nAge: " << person.age << endl;*/
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

	Time currentTime;

	int hour = 0;
	SetConsoleTextAttribute(console, 12);
	cout << "Enter curent hour: "; cin >> hour;	
	currentTime.hour = hour;

	int minute = 0;
	SetConsoleTextAttribute(console, 13);
	cout << "Enter curent minute: "; cin >> minute;
	currentTime.minute = minute;

	int second = 0;
	SetConsoleTextAttribute(console, 15);
	cout << "Enter curent second: "; cin >> second;
	currentTime.second = second;

	system("cls");
	SetConsoleTextAttribute(console, 10);
	currentTime.Show();	
	
	system("pause");
	return 0;
}