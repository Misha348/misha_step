#include <iostream>
#include <string>
#include <ctime>
using namespace std;

void FillArr(int *arr, int size, int numb)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = numb;
	}
}

void AddOneEl(int *&arr, int &size, int numb)
{
	int *newArr = new int[size + 1];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr[i];
	}	
	newArr[size] = numb;
	size++;

	delete[] arr;
	arr = newArr;
}

void PrintArr(int *arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "El [ " << i << " ] = " << arr[i] << endl;
	}
}

int main()
{
	int numb1 = 0;
	int numb = 0;

	bool exit = false;

	int size = 1;
	int *arr = new int[size];
	
	do
	{
		cout << "Enter positive number: ";
		cin >> numb1;
	} while (numb1 < 0);

	if (numb1 >= 0)	
	{		
		FillArr(arr, size, numb1);		

		while (exit = true)
		{
			cout << "Enter one more number: ";
			cin >> numb;			
			if (numb >= 0)
			{				
				AddOneEl(arr, size, numb);
			}
			if (numb < 0)
			{
				PrintArr(arr, size);
				exit = false;
				break;
			}
		}
	}	

	system("pause");
	return 0;
}