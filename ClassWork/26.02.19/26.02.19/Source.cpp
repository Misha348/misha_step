#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

void Test(int *pA)
{
	cout << "adress a = " << pA << endl;
}


int main()
{
	srand(unsigned(time(NULL)));
	int a = 10;
	int *pA = &a;
	cout << " a = " << a << endl;
	cout << "adress a = " << &a << endl;
	cout << "a = " << *pA << endl;
	Test(pA);

	int &Ra = a;
	cout << "Value &RA a = " << Ra << "\t &RA REF a = " << &Ra << endl;


	system("pause");
	return 0;
}