#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

void Dob(int &a, int &b, int &c)
{
	cout << "Dob = " << a * b * c << endl;
}

void Aver(int &a, int &b, int &c)
{
	cout << "Aver = " << (a + b + c) / 3 << endl;
}

void Min(int &a, int &b, int &c)
{
	int min = 0;

	if ((a < b) && (a < c))
	{
		min = a;
		cout << "Min = " << a << endl;
	}
	if ((b < a) && (b < c))
	{
		min = b;
		cout << "Min = " << b << endl;
	}
	if ((c < a) && (c < b))
	{
		min = c;
		cout << "Min = " << c << endl;
	}
}


int main()
{
	int a = 5, b = 7, c = 9;
	int &Ra = a;
	int &Rb = b;
	int &Rc = c;

	Dob(Ra, Rb, Rc);
	Aver(Ra, Rb, Rb);
	Min(Ra, Rb, Rc);


	system("pause");
	return 0;
}
