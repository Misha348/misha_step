# include <iostream>
# include <ctime>
using namespace std;

void Fill_Arr(int *arr1, int size)
{
	for (int i = 0; i < size; i++)
	{
		arr1[i] = rand() % 10;
	}
}

void Print_Arr(int *arr1, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "El [" << i << "] = " << arr1[i] << endl;
	}
}

// add last el
void Push_Back_El(int *&arr1, int &size, int val)
{
	int *newArr = new int[size + 1];

	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i];
	}
	newArr[size] = val;
	size++;

	delete[] arr1;
	arr1 = newArr;
}

// add front el
void Push_Front_El(int *&arr1, int &size, int val1)
{
	size++;
	int *newArr = new int[size + 1];
	newArr[0] = val1;
	for (int i = 1; i < size; i++)
	{
		newArr[i] = arr1[i - 1];
	}
	delete[] arr1;
	arr1 = newArr;
}

// delete pointed position el.
void Delete_El(int *&arr1, int &size, int pos)
{
	// int pos1 = pos - 1;
	int *newArr = new int[size];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i];
	}
	size--;
	for (int i = pos; i < size; i++) // it's better to use pos1 !!!
	{
		newArr[i] = newArr[i + 1];
	}
	delete[] arr1;
	arr1 = newArr;
}

// add pointed position el.
void Add_El(int *&arr1, int &size, int pos1, int val2)
{

	int *newArr = new int[size];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i];
	}
	size++;
	for (int i = 0; i < pos1; i++)
	{
		newArr[i] = arr1[i];
	}

	newArr[pos1] = val2;

	for (int i = pos1 + 1; i < size; i++)
	{
		newArr[i] = arr1[i - 1];
	}

	delete[] arr1;
	arr1 = newArr;
}

// delete last el.
void Delete_Last_El(int *&arr1, int &size)
{
	int *newArr = new int[size - 1];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i];
	}
	size--;
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i];
	}
	delete[] arr1;
	arr1 = newArr;
}

// delete first el.
void Delede_Front_El(int *& arr1, int &size)
{
	int *newArr = new int[size - 1];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i];
	}
	size--;
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i+1];
	}
	delete[] arr1;
	arr1 = newArr;
}

void Add_Block_Of_El(int *&arr1, int &size)
{
	cout << "Enter number of elements in block:";
	int n = 0;
	cin >> n;
	int *block = new int[n];
	cout << "Fill elements in block:" << endl;
	for (int i = 0; i < n; i++)
	{
		cin >> block[i];
	}

	cout << "In what position to add elements?: \n [ 1 ] - in first position.\n [ 2 ] - in pointed position.\n [ 3 ] - in last position" << endl;
	int choise = 0;
	cin >> choise;
	if (choise == 1)
	{	//add block at the begining	
		size += n;
		int *newArr = new int[size + n];
		for (int i = 0; i < n; i++)
		{
			newArr[i] = block[i];
		}
		for (int i = n; i < size; i++)
		{
			newArr[i] = arr1[i - n];
		}
		delete[] arr1;
		delete[] block;
		arr1 = newArr;
	}
	if (choise == 2)
	{	// add block in pointet position
		cout << " In what position input block?: ";
		int pos = 0;
		cin >> pos;
		int * newArr = new int[size];

		for (int i = 0; i < size; i++)
		{
			newArr[i] = arr1[i];
		}
		size += n;
		for (int i = 0; i < pos; i++)
		{
			newArr[i] = arr1[i];
		}

		for (int i = pos; i < pos + n; i++)
		{
			newArr[i] = block[i - pos];
		}

		for (int i = pos + n; i < size; i++)
		{
			newArr[i] = arr1[i - n];
		}
		delete[] arr1;
		delete[] block;
		arr1 = newArr;
	}
	//add block in the end.
	if (choise == 3)
	{
		int *newArr = new int[size + n];
		for (int i = 0; i < size; i++)
		{
			newArr[i] = arr1[i];
		}
		for (int i = size; i < size + n; i++)
		{
			newArr[i] = block[i - size];
		}
		size += n;
		delete[] arr1;
		delete[] block;
		arr1 = newArr;
	}
}

void Delete_Block_El(int *&arr1,int &size)
{
	cout << "Enter number of elements in block:";
	int n = 0;
	cin >> n;		

	cout << "From what position to delete elements?: \n [ 1 ] - from first position.\n [ 2 ] - from pointed position.\n [ 3 ] - from last position" << endl;
	int choise = 0;
	cin >> choise;	

	if(choise == 1)
	{		
		size -= n;
		int *newArr = new int[size];
		for (int i = 0; i < size; i++)
		{
			newArr[i] = arr1[i + n];
		}
		delete[] arr1;
		arr1 = newArr;
	}
	if (choise == 2)
	{
		cout << "Enter position, from which to delete block?:" << endl;
		int pos = 0;
		cin >> pos;

		size -= n;
		int *newArr = new int[size];
		for (int i = 0; i < pos; i++)
		{
			newArr[i] = arr1[i];
		}
		for (int i = pos; i < size; i++)
		{
			newArr[i] = arr1[i + n];
		}
		delete[] arr1;
		arr1 = newArr;
	}
	if (choise == 3)
	{
		size -= n;
		int *newArr = new int[size];
		for (int i = 0; i < size; i++)
		{
			newArr[i] = arr1[i];
		}
		delete[] arr1;
		arr1 = newArr;				
	}
}


void Menu()
{
	cout << "\n\tActions with array:\n\n[ 1 ] - add element in the end.\n[ 2 ] - add element in the front.\n[ 3 ] - delete pointed element.\n[ 4 ] - add element into the array.\n[ 5 ] - delete last elment.\n[ 6 ] - delete first element.\n[ 7 ] - input block of numbers. \n[ 8 ] - delete block of numbers."  << endl;
	cout << "[ 0 ] - Exit." << endl;
}	

int main()
{
	cout << "\t\t\t[ work with arrays ]" << endl;
	srand(unsigned(time(NULL)));
	int size = 0;
	cout << "Enter amount of elements in array: ";
	cin >> size;
	int *arr1 = new int[size];

	Fill_Arr(arr1, size);
	Print_Arr(arr1, size);
	cout << endl;

	int choise = 0;	
	bool exit = false;

	while (exit = true )
	{
		Menu();
		cin >> choise;
		if (choise == 1)
		{
			cout << "Enter one number: ";
			int val = 0;
			cin >> val;
			Push_Back_El(arr1, size, val);
			Print_Arr(arr1, size);
		}
		if (choise == 2)
		{
			cout << "Enter one number: ";
			int val1 = 0;
			cin >> val1;
			Push_Front_El(arr1, size, val1);
			Print_Arr(arr1, size);
		}
		if (choise == 3)
		{
			cout << "From what position delete element?: " << endl;
			int pos = 0;
			cin >> pos;
			Delete_El(arr1, size, pos);
			Print_Arr(arr1, size);
		}
		if (choise == 4)
		{
			cout << "In what position to add element?: " << endl;
			int pos1 = 0;
			cin >> pos1;
			cout << "Enter number for adding: ";
			int val2 = 0;
			cin >> val2;
			Add_El(arr1, size, pos1, val2);
			Print_Arr(arr1, size);
		}
		if (choise == 5)
		{
			Delete_Last_El(arr1, size);
			Print_Arr(arr1, size);
		}
		if (choise == 6)
		{
			Delede_Front_El(arr1, size);
			Print_Arr(arr1, size);
		}
		if (choise == 7)
		{			
			Add_Block_Of_El(arr1, size);
			Print_Arr(arr1, size);
		}
		if (choise == 8)
		{
			Delete_Block_El(arr1, size);
			Print_Arr(arr1, size);
		}
		if (choise == 0)
		{
			cout << "\tEnd." << endl;
			exit = false;	
			break;
		}		
	}
	system("pause");
	return 0;
}