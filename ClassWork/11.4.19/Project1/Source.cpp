#include <iostream>
#include <string>
#include <stdlib.h>;
#include <ctime>

using namespace std;

int Fill_Arr(int *ptr, int size)
{
	for (int i = 0; i < size; i++)
	{
		ptr[i] = rand() % 10;
	}
	return *ptr;
}

void Print_Arr(int *ptr, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "El [" << i << "] = " << ptr[i] << endl;
	}
}

void Add_El(int *&ptr, int size, int pos1, int val2)
{	
	int *tmpPtr = (int*)calloc(size+1, sizeof(int));	

	ptr = (int*)realloc(ptr, (size + 1) * sizeof(int));
	for (int i = 0; i < pos1; i++)
	{
		tmpPtr[i] = ptr[i];
	}
	tmpPtr[pos1] = val2;
	
	for (int i = pos1 + 1; i < size+1; i++)
	{
		tmpPtr[i] = ptr[i - 1];
	}		

	ptr = tmpPtr;
}

void AddElToEnd(int *&ptr, int &size, int val1)
{
	int *tmpPtr = (int*)calloc(size + 1, sizeof(int));
	ptr = (int*)realloc(ptr, (size + 1) * sizeof(int));
	for (int i = 0; i < size; i++)
	{
		tmpPtr[i] = ptr[i];
	}
	tmpPtr[size] = val1;
	ptr = tmpPtr;	
}

void DelElFromPos(int *&ptr, int &size, int pos1)
{
	int *tmpPtr = (int*)calloc(size, sizeof(int));	
	for (int i = 0; i < size; i++)
	{
		tmpPtr[i] = ptr[i];
	}
	ptr = (int*)realloc(ptr, (size - 1) * sizeof(int));
	for (int i = pos1; i < size; i++)
	{
		tmpPtr[i] = tmpPtr[i + 1];
	}
	ptr = tmpPtr;	
}


void Menu()
{
	cout << "\n\tActions with array:\n\n[ 1 ] - add element in the end.\n[ 2 ] - add element into the array.\n[ 3 ] - delete pointed element.\n[ 4 ] - shoe address of the array.\n[ 0 ] - Exit." << endl;
	
}

int main()
{
	cout << "\t\t\t[ work with arrays ]" << endl;
	srand(unsigned(time(NULL)));

	int size = 0;
	cout << "Enter amount of elements in array: ";
	cin >> size;	
	int choise = 0;
	bool exit = false;

	int *ptr = (int*)calloc(size, sizeof(int));

	Fill_Arr(ptr, size);
	Print_Arr(ptr, size);
	while (exit = true)
	{
		Menu();
		cin >> choise;
		
		if (choise == 1)
		{			
			cout << "Enter number for adding: ";
			int val1 = 0;
			cin >> val1;
			AddElToEnd(ptr, size, val1);
			size++;
			Print_Arr(ptr, size);
			
		}
		if (choise == 2)
		{
			cout << "In what position to add element?: " << endl;
			int pos1 = 0;
			cin >> pos1;
			cout << "Enter number for adding: ";
			int val2 = 0;
			cin >> val2;
			
			Add_El(ptr, size, pos1, val2);
			size++;
			Print_Arr(ptr, size);
		}
		if (choise == 3)
		{
			cout << "from what position delete element?: " << endl;
			int pos1 = 0;
			cin >> pos1;
			DelElFromPos(ptr, size, pos1);
			size--;
			Print_Arr(ptr, size);
		}
		if (choise == 4)
		{
			int addres = Fill_Arr(ptr, size);
			cout << "address of array = " << ptr << endl;
		}
		if (choise == 0)
		{
			cout << "\tEnd." << endl;
			exit = false;
			break;
		}
	}
	cout << endl;
	free(ptr);	

	system("pause");
	return 0;
}