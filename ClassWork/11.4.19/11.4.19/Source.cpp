#include <iostream>
#include <string>
#include <stdlib.h>;

using namespace std;

int main()
{		  // = new int;
	//int *ptr = (int *)malloc(sizeof(int)); // malloc produse memory
	//*ptr = 10;
	//cout << " *ptr = " << *ptr << " ptr = " << ptr << endl;
	//free(ptr);
	//cout << " *ptr = " << *ptr << " ptr = " << ptr << endl;
	cout << "-------------------------------" << endl;

	//int *ptr = (int *)malloc(2 * sizeof(int)); // malloc produse memory. all elements are - GARBEG !!
	//ptr[0] = 10;
	//ptr[1] = 20;
	//cout << " ptr[0] = " << ptr[0] << endl;
	//cout << " ptr[1] = " << ptr[1] << endl;	
	//free(ptr);
	cout << "-------------------------------" << endl;

	int count = 2;
	int *ptr = (int*)calloc(count, sizeof(int)); // all element = 0 !!!
	ptr[0] = 5;
	ptr[1] = 6;
	ptr[2] = 7;
	cout << " ptr[0] = " << ptr[0] << endl;
	cout << " ptr[1] = " << ptr[1] << endl;	
	ptr = (int*)realloc(ptr, 3 * sizeof(int));
	ptr[2] = 30;
	cout << " ptr[2] = " << ptr[2] << endl;
	free(ptr);


	system("pause");
	return 0;
}
