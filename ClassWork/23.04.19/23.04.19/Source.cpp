#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class Car
{
public:
	string model;
	string colour;
	string regNumber;
	float power;
	int mass;

	void ShowCarInfo()
	{
		cout << "Model - " << model << "\nColour - " << colour << "\nReg number - " << regNumber << "\nEngeen power - " << power << "\nMassa" << mass << endl;	
	}
};



int main()
{
	Car mers;
	mers.model = "A 140";
	mers.colour = "metallic";
	mers.regNumber = "we 121212";
	mers.power = 1.6;
	mers.mass = 1800;
	
	cout << "-------MERS----------" << endl;
	/*cout << "Model - " << mers.model << "\nColour - " << mers.colour << "\nReg number - " << mers.regNumber << "\nEngeen power - " << mers.power << "\nMassa" << mers.mass << endl;
	mers.model = "B 280";
	cout << "Model - " << mers.model << endl*/
	mers.ShowCarInfo();
	cout << "-----------------" << endl;
	mers.model = "B 280";
	mers.ShowCarInfo();
	cout << "\n-------- BMV ---------" << endl;

	Car bmw;
	bmw.model = "R 300";
	bmw.colour = "metallic - gold";
	bmw.regNumber = "UT 03030303";
	bmw.power = 2.6;
	bmw.mass = 20000;
	bmw.ShowCarInfo();
	system("pause");
	return 0;
}