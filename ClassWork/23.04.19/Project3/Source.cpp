#include <iostream>
#include <string>
#include <ctime>
using namespace std;

class Account
{
	string owner;
	int accNumber;
	int balance;
	string currency;
public:	
	void SetOwner(string newOvner)
	{
		owner = newOvner;	
	}
	string GetOwner()
	{
		return owner;	
	}

	void SetAccNumber(int newAccNumber)
	{
		accNumber = newAccNumber;
	}
	int GetAccNumber()
	{
		return accNumber;
	}

	void SetBalance(int newBalance)
	{
		balance = newBalance;
	}
	int GetBalance()
	{
		return balance;
	}

	void SetCurrency(string newCurrency)
	{
		currency = newCurrency;
	}
	string GetCurrency()
	{
		return currency;
	}

	void ShowAccData()
	{
		cout << "Account owner: " << owner << "\nAccount number: " << accNumber << "\nBalance: " << balance << "\nCurrency: " << currency << endl;
	}	
};

void Menu()
{
	cout << "\n\tchoose action: \n[ 1 ] - Add money to account.\n[ 2 ] - Get money from account.\n[ 3 ] - Show balance.\n[ 0 ] - Exit." << endl;
}


int main()
{	
	string owner;
	int accNumber;
	int balance;
	string currency;
	int choise = 0;
	bool exit = false;

	cout << "\t [ create bank account: ]" << endl;	
	
	Account acc1;
	cout << "Enter account owner: ";
	cin >> owner;
	acc1.SetOwner(owner);

	cout << "Enter account number: ";
	cin >> accNumber;
	acc1.SetAccNumber(accNumber);

	cout << "Enter account balance: ";
	cin >> balance;
	acc1.SetBalance(balance);

	cout << "Enter currency: ";
	cin >> currency;
	acc1.SetCurrency(currency);

	int addedMoney = 0;
	int getMonFromAcc = 0;

	while (exit != true)
	{
		Menu();
		cin >> choise;
		if (choise == 1)
		{
			cout << "Enter quantity of money for gaining balance: ";
			cin >> addedMoney;
			balance += addedMoney;
			acc1.SetBalance(balance);
		}
		if (choise == 2)
		{
			cout << "Enter quantity of money for gatting from account: ";
			cin >> getMonFromAcc;
			balance -= getMonFromAcc;
			acc1.SetBalance(balance);
		}
		if (choise == 3)
		{
			acc1.ShowAccData();
		}
		if (choise == 0)
		{
			exit = true;
			break;
		}
	}
	

	
	

	

	
	
	

	
	
	
		


	system("pause");
	return 0;
}