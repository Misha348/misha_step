#include <iostream>
#include <string>
#include <ctime>
using namespace std;

class Point
{
public:
	string pointName;
	int X;
	int Y;

	void ShowCoordinate()
	{
		cout << "Point - " << pointName << endl;
		cout << "(" << X << ", " << Y << ")" << endl;	
	}
};


int main()
{
	Point point1;
	cout << "Enter point name: "; cin >> point1.pointName;
	cout << "Enter X: "; cin >> point1.X;
	cout << "\nEnter Y: "; cin >> point1.Y;
	point1.ShowCoordinate();

	system("pause");
	return 0;
}