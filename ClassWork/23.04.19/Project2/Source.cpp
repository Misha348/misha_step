#include <iostream>
#include <string>
#include <ctime>
using namespace std;

class Car {
private:
	string model;
	float power;
	string engine;
	int year;

public:
	void ShowCarInfo() {
		cout << "Model: " << model << "\nPower: " << power << "\nEngine: " << engine << "\nYear: " << year << endl;
	}

	void SetModel(string newModel) {
		model = newModel;
	}

	string GetModel() {
		return model;
	}
};





int main()
{
	Car mazda;
	mazda.SetModel("Mazda");
	mazda.ShowCarInfo();

	cout << "GetModel = " << mazda.GetModel() << endl;

	mazda.SetModel("Mazda 3");
	cout << "GetModel = " << mazda.GetModel() << endl;

	system("pause");
	return 0;
}