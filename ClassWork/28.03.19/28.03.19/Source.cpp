#include <iostream>
#include <string>
using namespace std;

struct Person
{
	string name;
	string surname;
	unsigned short age;

	void ShowPersonInfo()
	{
		cout << "============= Show persons Info =================" << endl;
		cout << "Name: " << name << "\nSurname: " << surname << "\nAge: " << age << endl;	
	}
};

void FillPerson(Person *persons, int perCount)
{
	for (int i = 0; i < perCount; i++)
	{
		cout << "Enter name: ";
		cin >> persons[i].name;
		cout << "Enter surname: ";
		cin >> persons[i].surname;
		cout << "Enter age: ";
		cin >> persons[i].age;		
	}
}

void ShowPerson(Person *persons, int perCount)
{
	for (int  i = 0; i < perCount; i++)
	{
		persons[i].ShowPersonInfo();
	}
	cout << "------------------------" << endl;
}


int main()
{
	
	cout << "How many persons to create: " << endl;
	int perCount = 0;
	cin >> perCount;


	Person *persons = new Person[perCount];
	FillPerson(persons, perCount);
	ShowPerson(persons, perCount);
	

	system("pause");
	return 0;
}