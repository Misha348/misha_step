#include <iostream>
#include <windows.h>
# include <string>
#include <ctime>
using namespace std;

void Func(int *pA, int *pB)
{
	cout << "Func *pA, value = " << *pA << " *pA, address = " << pA << endl;
	cout << "Func *pB value = " << *pB << " *pB address = " << pB << endl;

	int temp = *pA;
	*pA = *pB;
	*pB = temp;
	cout << "<---------------After---------------------->" << endl;
	cout << "Func *pA, value = " << *pA << " *pA, address = " << pA << endl;
	cout << "Func *pB value = " << *pB << " *pB address = " << pB << endl;
}

void Foo(int a, int b) {
	cout << "Foo a value = " << a << " a address = " << &a << endl;
	cout << "Foo b value = " << b << " b address = " << &b << endl;
}


int main() 
{
	int a = 10, b = 20;
	cout << "a value = " << a << " a address = " << &a << endl;
	cout << "b value = " << b << " b address = " << &b << endl;
	cout << "===============================>" << endl;

	Foo(a, b);

	int *pA = &a;
	int *pB = &b;


	Func(pA, pB);

	cout << "-------------Result-------------" << endl;
	cout << "a value = " << a << " a address = " << &a << endl;
	cout << "b value = " << b << " b address = " << &b << endl;
	cout << "===============================>" << endl;


	/*int a = 10;

	cout << "a value = " << a << " a address = " << &a << endl;

	int *pA = &a;
	cout << "*pA  value = " << *pA << " *pA address = " << pA << endl;
	cout << "===================================>" << endl;
	*pA = 100500;
	cout << "a value = " << a << " a address = " << &a << endl;
	cout << "*pA  value = " << *pA << " *pA address = " << pA << endl;

	cout << "===================================>" << endl;
	int *pointer = &a;
	cout << "a value = " << a << " a address = " << &a << endl;
	cout << "*pA  value = " << *pA << " *pA address = " << pA << endl;
	cout << "*pointer  value = " << *pointer << " *pointer address = " << pointer << endl;*/



	system("pause");
	return 0;
}