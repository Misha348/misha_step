#include <iostream>
#include <windows.h>
# include <string>
#include <ctime>
using namespace std;

template < typename T1, typename T2, typename T3, typename T4>
void DobFunk(T1 *p1, T2 *p2, T3 *p3, T4 *pDOB)
{	
	*pDOB = (*p1) * (*p2) * (*p3);
}

template < typename T1, typename T2, typename T3, typename T4>
void Aver(T1 *p1, T2 *p2, T3 *p3, T4 * pAVER )
{	
	*pAVER = ((*p1) + (*p2) + (*p3)) / 3;	;	
}

template < typename T1, typename T2, typename T3, typename T4>
void Min(T1 *p1, T2 *p2, T3 *p3, T4 *pMIN)
{
	if ((*p1 < *p2) && (*p1 < *p3))
	{
		*pMIN = *p1;
	}
	if ((*p2 < *p1) && (*p2 < *p3))
	{
		*pMIN = *p2;
	}
	if ((*p3 < *p1) && (*p3 < *p2))
	{
		*pMIN = *p3;
	}
}

int main()
{
	int dob = 0;
	int *pDOB = &dob;	

	double aver = 0;
	double *pAVER = &aver;

	int min = 0;
	int *pMIN = &min;

	int a = 0;
	cout << "1 number: ";
	cin >> a;
	int *p1 = &a;

	int b = 0;
	cout << "2 number: ";
	cin >> b;
	int *p2;
	p2 = &b;


	int c = 0;
	cout << "3 number: ";
	cin >> c;
	int *p3;
	p3 = &c;

	DobFunk(p1, p2, p3, pDOB);
	cout << "Dobutoc = " << dob << endl;

	Aver(p1, p2, p3, pAVER);
	cout << "Seredne = " << aver << endl;

	Min(p1, p2, p3, pMIN);
	cout << "Min = " << min << endl;


	system("pause");
	return 0;
}