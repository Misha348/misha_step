#include <iostream>
#include <ctime>;
#include <windows.h>
using namespace std;


//template <typename T1, typename T2>
//
//T1 Sum(T1 a, T2 b)
//{
//	/*cout << a << endl;
//	cout << b << endl;*/
//	return a + b;
//}

//template <typename T1, typename T2>
//T1 Sum(T1 a, T2 b, T1 c)
//{
//	/*cout << a << endl;
//	cout << b << endl;*/
//	return a + b + c;
//}
//--------------------------------------------------------

const int ROW = 4;
const int COL = 3;
const int ROW1 = 5;
const int COL1 = 7;

// fil int 3x4
void FillArr(int arr[ROW][COL])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			arr[i][j] = rand() % 29 + 1;			
		}
	}
}
// fil double 3x4
void FillArr(double arr[ROW][COL])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			arr[i][j] = (double)(rand()) / RAND_MAX * (30 - (1)) + (1);
		}
	}
}
// fil int 7x5
void FillArr(int arr[ROW1][COL1])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW1; i++)
	{
		for (int j = 0; j < COL1; j++)
		{
			arr[i][j] = rand() % 29 + 1;
		}
	}
}
// fil double 7x5
void FillArr(double arr[ROW1][COL1])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW1; i++)
	{
		for (int j = 0; j < COL1; j++)
		{
			arr[i][j] = (double)(rand()) / RAND_MAX * (30 - (1)) + (1);
		}
	}
}
// Print int 3x4
template <typename T1>
void PrintArr(T1 arr[ROW][COL])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{			
			cout << arr[i][j] << "\t";
			Sleep(100);
		}
		cout << endl;
	}
}
// Print int 7x5
template <typename T1>
void PrintArr(T1 arr[ROW1][COL1])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW1; i++)
	{
		for (int j = 0; j < COL1; j++)
		{
			cout << arr[i][j] << "\t";
			Sleep(100);
		}
		cout << endl;
	}
}
// Couple => 0; int 3x4
void CoupleToNull(int arr[ROW][COL])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			if (arr[i][j] % 2 == 0)
			{
				arr[i][j] = 0;
			}
			cout << arr[i][j] << "	";
			Sleep(100);
		}
		cout << endl;
	}
}
// Couple => 0; double 3x4
void CoupleToNull(double arr[ROW][COL])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			int a = static_cast<int>(arr[i][j]);
			
			if (a % 2 == 0)
			{
				arr[i][j] = 0;
			}
			cout << arr[i][j] << "	";
			Sleep(100);
		}
		cout << endl;
	}
}
// Couple => 0; int 7x5
void CoupleToNull(int arr[ROW1][COL1])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW1; i++)
	{
		for (int j = 0; j < COL1; j++)
		{
			if (arr[i][j] % 2 == 0)
			{
				arr[i][j] = 0;
			}
			cout << arr[i][j] << "	";
			Sleep(100);
		}
		cout << endl;
	}
}
// Couple => 0; double 7x5
void CoupleToNull(double arr[ROW1][COL1])
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < ROW1; i++)
	{
		for (int j = 0; j < COL1; j++)
		{
			int a = static_cast<int>(arr[i][j]);
			if (a % 2 == 0)
			{
				arr[i][j] = 0;
			}
			cout << arr[i][j] << "	";
			Sleep(100);
		}
		cout << endl;
	}
}


int main()
{
	cout << "\tWhat kind of array to use:\t[ 1 ]  - INTEGER array.\t[ 2 ] - DOUBLE array." << endl;
	int choise = 0;
	cin >> choise;
	if (choise == 1)
	{
		int arr[ROW][COL];
		cout << "\tFirst array - [ 3x4 ]\n" << endl;
		FillArr(arr);
		PrintArr(arr);
		cout << "\n\n";
		cout << "Couple numbers => 0\n" << endl;
		CoupleToNull(arr);
	
		int arr1[ROW1][COL1];
		cout << "\n\tSecond array - [ 7x5 ]\n" << endl;
		FillArr(arr1);
		PrintArr(arr1);
		cout << "\n\n";
		cout << "Couple numbers => 0\n" << endl;
		CoupleToNull(arr1);
		cout << "\n\n";
	}
	if (choise == 2)
	{
		double arr[ROW][COL];
		cout << "\tFirst array - [ 3x4 ]\n" << endl;
		FillArr(arr);
		PrintArr(arr);
		cout << "\n\n";
		cout << "Couple numbers => 0\n" << endl;
		CoupleToNull(arr);

		double arr1[ROW1][COL1];
		cout << "\n\tSecond array - [ 7x5 ]\n" << endl;
		FillArr(arr1);
		PrintArr(arr1);
		cout << "\n\n";
		cout << "Couple numbers => 0\n" << endl;
		CoupleToNull(arr1);
		cout << "\n\n";
	}



	/*Sum(5, 6);
	cout << " " << endl;

	Sum(5.8, 4.6);
	cout << " " << endl;

	Sum("Bill", "Kolya");
	cout << " " << endl;

	Sum(5, 6.7);
	cout << " " << endl;

	Sum(6.7, 5);
	cout << " " << endl;*/

	/*cout << Sum(5, 5.2, 10) << endl;*/

	


	system("pause");
	return 0;
}