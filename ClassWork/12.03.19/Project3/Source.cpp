void PushRow(int **&array, int col, int row, int index)
{
	row++;
	int **newArr = new int*[row];
	for (int i = 0; i < row; i++)
	{
		newArr[i] = new int[col];
	}

	for (int i = 0; i < row; i++)
	{
		if (i < index)
		{
			for (int j = 0; j < col; j++)
			{
				newArr[i][j] = array[i][j];
			}
		}
		else if (i == index)
		{
			for (int j = 0; j < col; j++)
			{
				newArr[i][j] = rand() % 10;
			}
		}
		else if (i > index)
		{
			for (int j = 0; j < col; j++)
			{
				newArr[i][j] = array[i - 1][j];
			}
		}
	}
	/*��� ������� ����������� ��������� �����*/
	array = newArr;
}