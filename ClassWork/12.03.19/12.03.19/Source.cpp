#include <iostream>;
#include <ctime>;
using namespace std;

int main()
{
	srand(unsigned(time(NULL)));
	int cols = 0;
	int rows = 0;
	cout << "Enter rows:";
	cin >> rows;
	cout << "Enter cols:";
	cin >> cols;

	int **arr = new int *[rows];

	for (int i = 0; i < rows; i++)
	{
		arr[i] = new int[cols];
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			arr[i][j] = rand() % 10;
		}		
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			cout << arr[i][j] << "\t";
		}
		cout << endl;
	}

	for (int i = 0; i < rows; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;

	system("pause");
	return 0;
}

