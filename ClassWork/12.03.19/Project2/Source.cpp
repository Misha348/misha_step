#include <iostream>;
#include <ctime>;
using namespace std;

void Fill_Arr(int **arr, int rows, int cols)
{
	for (int i = 0; i < rows; i++)
	{
		arr[i] = new int[cols];
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			arr[i][j] = rand() % 10;
		}
	}
}

void Print_Arr(int **arr, int rows, int cols)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			cout << arr[i][j] << "\t";
		}
		cout << endl;
	}
	/*for (int i = 0; i < rows; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;*/
}

//	add new row;
void Add_Row(int **&arr, int rows, int cols)
{
	rows++;
	int **newArr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		newArr[i] = new int[cols];
	}
	for (int i = 0; i < (rows - 1); i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i][j];
		}
	}
	for (int i = (rows - 1); i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = rand() % 10;
			/*cout << newArr[i][j] << " ";*/
		}
	}

	/*for (int i = 0; i < rows -1 ; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;*/
	arr = newArr;
}

//	add new row by pointed possition;
void Add_Row_By_Pos(int **&arr, int rows, int cols, int pos)
{
	int **newArr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		newArr[i] = new int[cols];
	}
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i][j];
		}
	}
	rows++;
	for (int i = 0; i < pos; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i][j];
		}
	}
	for (int i = pos; i < pos + 1; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = rand() % 10;
		}
	}
	for (int i = pos + 1; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i - 1][j];
		}
	}
	for (int i = 0; i < rows; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;
	arr = newArr;
}

//	delete row by pointed possition;
void Delete_Row_By_Pos()
{
}

//	add new column;
void Add_Col()
{
}

//	add new column by pointed possition;
void Add_Col_By_Pos()
{
}

//	delete column by pointed possition;
void Delete_Col_By_Pos()
{
}

void Menu()
{
	cout << "\t\t [ Actions with twodimentional array ]\n [ 1 ] - Add new row.\n [ 2 ] - Add new row by pointed possition. \n [ 3 ] - Delete row by pointed possition || \n [ 4 ] - Add new column.\n [ 5 ] - Add new column by pointed possition. \n [ 6 ] - Delete column by pointed possition. \n[ 0 ] - Exit." << endl;
}

int main()
{
	srand(unsigned(time(NULL)));
	int cols = 0;
	int rows = 0;
	cout << "Enter rows:";
	cin >> rows;
	cout << "Enter cols:";
	cin >> cols;
	int **arr = new int *[rows];

	Fill_Arr(arr, rows, cols);
	Print_Arr(arr, rows, cols);

	bool exit = false;
	int choise = 0;
	while (exit = true)
	{
		Menu();
		cin >> choise;
		if (choise == 1)
		{
			Add_Row(arr, rows, cols);
			rows++;
			Print_Arr(arr, rows, cols);
		}
		if (choise == 2)
		{
			int pos = 0;
			cout << "In what possition to add new row? ";
			cin >> pos;
			Add_Row_By_Pos(arr, rows, cols, pos);
			Print_Arr(arr, rows, cols);
		}
		if (choise == 3)
		{
		}
		if (choise == 4)
		{
		}
		if (choise == 5)
		{
		}
		if (choise == 6)
		{
		}
		if (choise == 0)
		{
			cout << "The end." << endl;
			exit = false;
			break;
		}
	}
	system("pause");
	return 0;
}