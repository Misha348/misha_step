# include <iostream>
# include <ctime>

using namespace std;

void Fill_Arr(int *const arr, const int size)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = rand() % 10;
	}
}

void Print_Arr(int *const arr, const int size) 
{
	cout << "arr address " << arr << endl;
	for (int i = 0; i < size; i++) 
	{
		cout << "Arr [" << i << "] = " << arr[i] << "\t" << arr + i << endl;
	}
}

int main() {
	srand(unsigned(time(NULL)));
	//=====================Demo 1 ========================>
	/*int size = 0;
	cout << "Enter arr size " << endl;
	cin >> size;
	cout << "============================>" << endl;
	int *arr = new int[size];

	for (int i = 0; i < size; i++) {
	arr[i] = rand() % 10;
	cout << "Arr [" << i << "] = " << arr[i] << "\t" << arr + i << endl;
	}
	delete[] arr;*/
	//=====================Demo 1 ========================>
	//=====================Demo 2 ========================>
	/*int size = 0;
	cout << "Enter arr size " << endl;
	cin >> size;
	cout << "============================>" << endl;
	int *arr = new int[size];

	cout << "arr address " << arr << endl;
	Fill(arr, size);
	Print(arr, size);

	delete[] arr;*/
	//=====================Demo 2 ========================>
	//=====================Demo 3 ========================>
	int size = 5;
	int *arr1 = new int[size];
	int *arr2 = new int[size];

	Fill_Arr(arr1, size);
	Fill_Arr(arr2, size);
	cout << "Arr1========================>" << endl;
	Print_Arr(arr1, size);
	cout << "Arr2========================>" << endl;
	Print_Arr(arr2, size);

	delete[] arr1;
	arr1 = new int[size];

	for (int i = 0; i < size; i++)
	{
		arr1[i] = arr2[i];
	}

	cout << "Arr1========================>" << endl;
	Print_Arr(arr1, size);
	cout << "Arr2========================>" << endl;
	Print_Arr(arr2, size);




	delete[] arr1;
	delete[] arr2;
	//=====================Demo 3 ========================>

	system("pause");
	return 0;
}
