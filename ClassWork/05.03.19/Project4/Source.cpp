#include <iostream>

using namespace std;

int main()
{
	int N, pos;
	cout << "Enter size of array:" << endl;
	cout << "N = ";
	cin >> N;
	int* A = new int[N];
	cout << "fill array:" << endl;
	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}
	cout << "fill index to delete: ";
	cin >> pos;
	for (int i = pos; i < N - 1; i++) //��������� ����� ������� � �������� �������
	{
		A[i] = A[i + 1]; //�������� ��������
	}
	A[N - 1] = 0; //�������� ��������� ������� (����� � ����� ������������)
	cout << "array after deleting:" << endl;
	for (int i = 0; i < N - 1; i++) //��������� ������ ������� �� �������
	{
		cout << A[i] << " "; //������� ������ ����� �������� ��������
	}
	cout << endl;
	delete[] A;
	system("pause");
	return 0;
}