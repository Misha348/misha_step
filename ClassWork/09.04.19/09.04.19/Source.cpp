#include <iostream>
#include <string>
#include "Sum.h"
#include "Dif.h"
#include "Person.h"
using namespace std;

int main()
{
	cout << Sum(5, 2) << endl;
	cout << Dif(4.6, 7.9) << endl;
	cout << "-------------------------" << endl;

	Person person;
	person.name = "Bill";
	person.surname = "Jackson";
	person.age = 18;
	person.PrintInfo();


	system("pause");
	return 0;
}