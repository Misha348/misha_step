#pragma once

void FillArr(int *arr, int size);

void PrintArr(int *arr, int size);

void AddElToLastPos(int *&arr, int &size, int val);

void DelePointPosEl(int *&arr, int &size, int pos);

void AddElToPointPos(int *&arr, int &size, int pos1, int val2);

void Menu();