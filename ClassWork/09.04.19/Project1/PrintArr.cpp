#include "FuncForArr.h"
#include <iostream>
using namespace std;

void PrintArr(int *arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "El [" << i << "] = " << arr[i] << endl;
	}
}