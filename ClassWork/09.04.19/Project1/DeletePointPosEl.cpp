#include "FuncForArr.h"
#include <iostream>
using namespace std;

void DelePointPosEl(int *&arr, int &size, int pos)
{
	// int pos1 = pos - 1;
	int *newArr = new int[size];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr[i];
	}
	size--;
	for (int i = pos; i < size; i++) // it's better to use pos1 !!!
	{
		newArr[i] = newArr[i + 1];
	}
	delete[] arr;
	arr = newArr;
}