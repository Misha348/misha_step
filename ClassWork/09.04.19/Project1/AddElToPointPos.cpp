#include "FuncForArr.h"
#include <iostream>
using namespace std;

void AddElToPointPos(int *&arr, int &size, int pos1, int val)
{
	int *newArr = new int[size];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr[i];
	}
	size++;
	for (int i = 0; i < pos1; i++)
	{
		newArr[i] = arr[i];
	}

	newArr[pos1] = val;

	for (int i = pos1 + 1; i < size; i++)
	{
		newArr[i] = arr[i - 1];
	}

	delete[] arr;
	arr = newArr;
}