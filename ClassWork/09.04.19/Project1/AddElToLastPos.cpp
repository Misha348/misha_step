#include "FuncForArr.h"
#include <iostream>
using namespace std;

void AddElToLastPos(int *&arr, int &size, int val)
{
	int *newArr = new int[size + 1];

	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr[i];
	}
	newArr[size] = val;
	size++;

	delete[] arr;
	arr = newArr;
}