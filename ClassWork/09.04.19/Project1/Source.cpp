#include <iostream>
#include <string>
#include <ctime>
#include "FuncForArr.h"


using namespace std;

int main()
{	
	srand(unsigned(time(NULL)));
	int size = 0;
	cout << "Enter size of array: "; cin >> size;
	int *arr = new int[size];
	bool exit = false;

	FillArr(arr, size);
	PrintArr(arr, size);
	//Menu();	
	while (exit != true)
	{
		Menu();
		int choise = 0;
		cin >> choise;
		if (choise == 1)
		{
			int val = 0;
			cout << "Enter value: "; cin >> val;
			AddElToLastPos(arr, size, val);
			PrintArr(arr, size);
		}
		if (choise == 2)
		{
			int val = 0;
			int pos = 0;
			cout << "Enter value: "; cin >> val;
			cout << "Enter possition: "; cin >> pos;

			AddElToPointPos(arr, size, pos, val);
			PrintArr(arr, size);
		}
		if (choise == 3)
		{
			int pos = 0;
			cout << "Enter possition to delete: "; cin >> pos;
			DelePointPosEl(arr, size, pos);
			PrintArr(arr, size);
		}
		if (choise == 0)
		{
			exit = true;
			break;
		}
	}
	system("pause");
	return 0;
}
