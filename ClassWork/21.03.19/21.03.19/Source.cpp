#include <iostream>;
#include <ctime>;
#include <string>;
using namespace std;

string GetDataFromPB()
{
	return "Data from PB";
}

string GetDataFromNBU()
{
	return "Data from NBU";
}

string GetDataFromSwB()
{
	return "Data from SwB";
}

void ShowInfo(string(*funcPtr)())
{
	cout << funcPtr() << endl;	
}


int main()
{	
	ShowInfo(GetDataFromSwB);

	system("pause");
	return 0;
}