#include <iostream>;
#include <ctime>;
#include <string>;
using namespace std;

void FillArr(int *arr, int SIZE)
{
	for (int i = 0; i < SIZE; i++)
	{
		arr[i] = rand() % 20 + (-10);
	}
}

void PrintArr(int *arr, int SIZE)
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << "El. [ " << i << " ] = " << arr[i] << endl;
	}
}

bool Biger(int digit1, int digit2)
{
	
	if (digit1 > digit2)
	{
		return true;
	}
	else if (digit1 < digit2)
	{	
		return false;
	}	
}

bool Smaller(int digit1, int digit2)
{
	if (digit1 < digit2)
	{
		return true;
	}
	else if (digit1 > digit2)
	{
		return false;
	}
}

void SortUp(int arr[], int SIZE)
{
	for (int i = 0; i < (SIZE - 1); i++)
	{
		for (int j = 0; j < SIZE; i++)
		{
			if (arr[i] > arr[j])
			{
				int tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
			}
		}
	}
	for (int i = 0; i < SIZE; i++)
	{
		cout << "El. [ " << i << " ] = " << arr[i] << endl;

	}
}

void SortDown(int arr[], int SIZE)
{
	for (int i = 0; i < (SIZE - 1); i++)
	{
		for (int j = 0; j < SIZE; i++)
		{
			if (arr[i] < arr[j])
			{
				int tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
			}
		}
	}
	for (int i = 0; i < SIZE; i++)
	{
		cout << "El. [ " << i << " ] = " << arr[i] << endl;
	
	}
			
}

void SortArr(int *arr, int SIZE, bool(*f)(int digit1, int digit2))
{
	int digit1 = 0;
	int digit2 = 0;

	cout << "Enter 1 digit: " << endl;
	cin >> digit1;
	cout << "Enter 2 digit: " << endl;
	cin >> digit2;

	if (f(digit1, digit2))
	{
		SortUp(arr, SIZE);
	}
	else
	{
		SortDown(arr, SIZE);
	}
		
		
}


int main()
{
	srand(unsigned(time(NULL)));
	const int SIZE = 10;
	int arr[SIZE];
	FillArr(arr, SIZE);
	PrintArr(arr, SIZE);

	int digit1 = 0;
	int digit2 = 0;	

	/*cout << "\n";
	cout << Biger(digit1, digit2) << endl;
	cout << Smaller(digit1, digit2) << endl;*/

	cout << "\n";
	SortArr(arr, SIZE, Biger);
	/*SortArrDown(arr, SIZE, Smaller);*/





	system("pause");
	return 0;
}