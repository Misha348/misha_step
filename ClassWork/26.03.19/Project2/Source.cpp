﻿#include <iostream>
#include <string>
using namespace std;

//Створити структуру, яка описує тварину(назва, клас, кличка).Створити функції для роботи з цією структурою
//(заповнення об’єкта, вивід на екран даних про об’єкт, функція «подати голос»).

struct Animal
{
	string type;
	string breed;
	string name;		

	void ShowInfo(string type, string breed, string name)
	{
		cout << "Type - " << type << "\nBreed - " << breed << "\nName - " << name << endl;
	}
	void MakeNoise(string noise)
	{
		cout << "produse noise - " << noise << endl;

	}
};


int main()
{
	Animal cat;	
	cat.ShowInfo("mammal", "british", "Tom");
	cat.MakeNoise("meow");


	system("pause");
	return 0;
}