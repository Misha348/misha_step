#include <iostream>;
#include <ctime>;
#include <string>;
using namespace std;

struct Dog
{
	string name;
	string breed;
	int age;
	string description;

	void ShowInfo()
	{
		cout << "name = " << name << "\nbreed = " << breed << "\nage = " << age << endl;			
	}	
	

	void ShowDescription(string des)
	{
		description = des;
		cout << "\ndescription = " << description << endl;
	}
};


int main()
{
	Dog tuzic;
	tuzic.name = "tuzic";
	tuzic.breed = "dwordog";
	tuzic.age = 7;
	tuzic.ShowInfo();
	cout << "-----------------------------" << endl;
	tuzic.age = 10;
	tuzic.ShowInfo();
	cout << "-----------------------------" << endl;

	Dog bobic;
	bobic.name = "bobic";
	bobic.breed = "dwordog_2";
	bobic.age = 10;
	bobic.ShowInfo();
	cout << "-----------------------------" << endl;
	string des;
	cout << "Enter description " << endl;
	cin >> des;	
	bobic.ShowDescription(des);


	system("pause");
	return 0;
}