#include <iostream>
#include <windows.h>
# include <string>
#include <ctime>
using namespace std;

template <typename T1, typename T2>
void FillCardNum(T1 arrCard[], const T2 SIZE)
{
	cout << "\t[ Your cards ]\n" << endl;
	for (int i = 0; i < SIZE; i++)
	{		
		cout << "Enter number of [" << i + 1 << "] card:";
		cin >> arrCard[i];
	}
}

template <typename T1, typename T2>
void FillPasswcard(T1 passwCard[], const T2 SIZE)
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << "Enter password of [" << i + 1 << "] card:";
		cin >> passwCard[i];
	}
}

int FillTelNum(int number)
{
	cout << "Enter your tel. number in format: 380xxxxxxxxx" << endl;
	cin >> number;
	return number;
}


int main()
{
	const int SIZE1 = 3;
	int arrCard[SIZE1];
	cout << "\t\t [ REGISTRATION ]\n\n" << endl;
	FillCardNum(arrCard, SIZE1);	

	int passwCard[SIZE1];
	FillPasswcard(passwCard, SIZE1);

	int number = 0;
	int telNum = FillTelNum(number);
	system("cls");

	system("pause");
	return 0;
}