﻿#include <iostream>
#include <windows.h>
# include <string>
#include <ctime>
using namespace std;


// 10 масивів

template <typename T1, typename T2>
void FillArr(T1 arr[], const T2 SIZE)
{	
	for (int i = 0; i < SIZE; i++)
	{
		arr[i] = rand() % 20 + (-10);
	}	
}

template <typename T1, typename T2>
void PrintArr(T1 arr[], const T2 SIZE)
{	
	int sum = 0;
	for (int i = 0; i < SIZE; i++)

	{			
		cout << "["<< i + 1 << " el.] - " << arr[i] << endl;			
	}	
}

int SumArr(int arr[], const int SIZE)
{
	int sum = 0;
	for (int i = 0; i < SIZE; i++)
	{
		sum += arr[i];
	}
	cout << "_________________" << "[ SUM ] = " << sum << "\n" << endl;
	return sum;
}


int main()
{
	srand(unsigned(time(NULL)));

	const int SIZE = 10;


	int arr1[SIZE];		
	FillArr(arr1, SIZE);
	cout << "\tARRAY-1" << endl;
	PrintArr(arr1, SIZE);
	int sum1 = SumArr(arr1, SIZE);

	int arr2[SIZE];
	FillArr(arr2, SIZE);
	cout << "\tARRAY-2" << endl;
	PrintArr(arr2, SIZE);
	int sum2 = SumArr(arr2, SIZE);

	int arr3[SIZE];
	FillArr(arr3, SIZE);
	cout << "\tARRAY-3" << endl;
	PrintArr(arr3, SIZE);
	int sum3 = SumArr(arr3, SIZE);

	int arr4[SIZE];
	FillArr(arr4, SIZE);
	cout << "\tARRAY-4" << endl;
	PrintArr(arr4, SIZE);
	int sum4 = SumArr(arr4, SIZE);

	int arr5[SIZE];
	FillArr(arr5, SIZE);
	cout << "\tARRAY-5" << endl;
	PrintArr(arr5, SIZE);
	int sum5 = SumArr(arr5, SIZE);

	int arr6[SIZE];
	FillArr(arr6, SIZE);
	cout << "\tARRAY-6" << endl;
	PrintArr(arr6, SIZE);
	int sum6 = SumArr(arr6, SIZE);

	int arr7[SIZE];
	FillArr(arr7, SIZE);
	cout << "\tARRAY-7" << endl;
	PrintArr(arr7, SIZE);
	int sum7 = SumArr(arr7, SIZE);

	int arr8[SIZE];
	FillArr(arr8, SIZE);
	cout << "\tARRAY-8" << endl;
	PrintArr(arr8, SIZE);
	int sum8 = SumArr(arr8, SIZE);

	int arr9[SIZE];
	FillArr(arr9, SIZE);
	cout << "\tARRAY-9" << endl;
	PrintArr(arr9, SIZE);
	int sum9 = SumArr(arr9, SIZE);

	int arr10[SIZE];
	FillArr(arr10, SIZE);
	cout << "\tARRAY-10" << endl;
	PrintArr(arr10, SIZE);
	int sum10 = SumArr(arr10, SIZE);

	
	int arrOfSum[SIZE]{sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, sum9, sum10};
	int MAX = 0;
	int positionOfMaxArr = 0;

	for (int i = 0; i < SIZE; i++)
	{
		if (arrOfSum[i] > MAX)
		{
			MAX = arrOfSum[i];
			positionOfMaxArr = i + 1;
		}		
	}
	cout << "Max sum is in array  [" << positionOfMaxArr <<"]\n" << endl;	
	
	if (positionOfMaxArr == 1)
	{
		PrintArr(arr1, SIZE);
		cout << sum1 << endl;
	}
	if (positionOfMaxArr == 2)
	{
		PrintArr(arr2, SIZE);
		cout << sum2 << endl;
	}
	if (positionOfMaxArr == 3)
	{
		PrintArr(arr3, SIZE);
		cout << sum3 << endl;
	}
	if (positionOfMaxArr == 4)
	{
		PrintArr(arr4, SIZE);
		cout << sum4 << endl;
	}
	if (positionOfMaxArr == 5)
	{
		PrintArr(arr5, SIZE);
		cout << sum5 << endl;
	}
	if (positionOfMaxArr == 6)
	{
		PrintArr(arr6, SIZE);
		cout << sum6 << endl;
	}
	if (positionOfMaxArr == 7)
	{
		PrintArr(arr7, SIZE);
		cout << sum7 << endl;
	}
	if (positionOfMaxArr == 8)
	{
		PrintArr(arr8, SIZE);
		cout << sum8 << endl;
	}
	if (positionOfMaxArr == 9)
	{
		PrintArr(arr9, SIZE);
		cout << sum9 << endl;
	}
	if (positionOfMaxArr == 10)
	{
		PrintArr(arr10, SIZE);
		cout << sum10 << endl;
	}


	system("pause");
	return 0;
}