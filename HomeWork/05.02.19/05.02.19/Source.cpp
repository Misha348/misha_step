#include <iostream>
#include <ctime>;
#include <windows.h>
using namespace std;

// fill int
void FillArr(int arr[], const int SIZE)
{	
	srand(unsigned(time(NULL)));
	for (int i = 0; i < SIZE; i++)
	{
		arr[i] = rand() % 80 + (-26);		
	}
}
// fill double
void FillArr(double arr[], const int SIZE)
{
	srand(unsigned(time(NULL)));
	for (int i = 0; i < SIZE; i++)
	{
		arr[i] =  (double)(rand())/RAND_MAX * (54 - (-26)) + (-26);
	}
}

//  print int
void PrintArr(int arr[], const int SIZE)
{
	int sum = 0;
	srand(unsigned(time(NULL)));
	for (int i = 0; i < SIZE; i++)
	{		
		cout << i + 1 << " - " << arr[i] << endl;
		Sleep(100);
		sum += arr[i];		
	}
	cout << "\t[ SUM ] = " << sum;
}
//  print double
void PrintArr(double arr[], const int SIZE)
{
	double sum = 0;
	srand(unsigned(time(NULL)));
	for (int i = 0; i < SIZE; i++)
	{		
		cout << i + 1 << " - " << arr[i] << endl;
		Sleep(100);
		sum += arr[i];
	}
	cout << "\t[ SUM ] = " << sum;
}

// MIN int
void FindMin(int arr[], const int SIZE)
{
	int min = 0;
	int cut = 0;
	cout << "Enter what position of element will be last in new array. " << endl;
	cin >> cut;
	srand(unsigned(time(NULL)));
	for (int i = 0; i < cut; i++)
	{
		/*arr[i] = rand() % 80 + (-26);*/
		cout << arr[i] << " ";
		Sleep(100);
		if (arr[i] < min)
		{
			min = arr[i];
		}
	}
	cout << "\nMIN = " << min << endl;
}
// MIN double
void FindMin(double arr[], const int SIZE)
{
	double min = 0;
	int cut = 0;
	cout << "Enter what position of element will be last in new array. " << endl;
	cin >> cut;
	srand(unsigned(time(NULL)));
	for (int i = 0; i < cut; i++)
	{		
		cout << arr[i] << " ";
		Sleep(100);
		if (arr[i] < min)
		{
			min = arr[i];
		}
	}
	cout << "\nMIN = " << min << endl;
}

int main()
{	
	const int SIZE = 7;	
	const int SIZE2 = 10;	

	cout << "\nHow to fill arrays: \t [ 1 ] - with Integer\t[ 2 ] with Fractal" << endl;
	int choise1 = 0;
	cin >> choise1;
	if (choise1 == 1)
	{
		int arr[SIZE];
		FillArr(arr, SIZE);
		PrintArr(arr, SIZE);

		cout << "\n" << endl;

		int arr1[SIZE2];
		FillArr(arr1, SIZE2);
		PrintArr(arr1, SIZE2);

		cout << "\n\nWhat array to use : \t[ 1 ] - First\t[ 2 ] - Second" << endl;
		int choise = 0;
		cin >> choise;
		if (choise == 1)
		{
			FindMin(arr, SIZE);
		}
		if (choise == 2)
		{
			FindMin(arr1, SIZE2);
		}
	}
	if (choise1 == 2)
	{
		double arr[SIZE];
		FillArr(arr, SIZE);
		PrintArr(arr, SIZE);

		cout << "\n" << endl;

		double arr1[SIZE2];
		FillArr(arr1, SIZE2);
		PrintArr(arr1, SIZE2);

		cout << "\n\nWhat array to use : \t[ 1 ] - First\t[ 2 ] - Second" << endl;
		int choise = 0;
		cin >> choise;
		if (choise == 1)
		{
			FindMin(arr, SIZE);
		}
		if (choise == 2)
		{
			FindMin(arr1, SIZE2);
		}
	}
	

	system("pause");
	return 0;
}

