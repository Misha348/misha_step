#include <iostream>;
#include <ctime>;
using namespace std;

//�������� ������� ��� ������ � ��������� ����������� ������� :
//a) ������� ��������� ���������� ������ ��������� ������ � ���� ���������� ����������� ������� � ��������[-12..56].������� ������� ������ ���������� ������.
//b) ������ ������
//c) ���������� ������ ����� ���������.������� ������ ������ ������, ����� �� ������� ��� ����������.

int Fill_Arr(int *arr1, int size)
{
	for (int i = 0; i < size; i++)
	{
		arr1[i] = rand()% 68 +(-12);
	}
	return *arr1;
}

void Print_Arr(int *arr1, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "El [" << i << "] = " << arr1[i] << endl;
	}
}

void Push_Back_El(int *&arr1, int &size, int val)
{
	int *newArr = new int[size + 1];

	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr1[i];
	}
	newArr[size] = val;
	size++;

	delete[] arr1;
	arr1 = newArr;
}

int main()
{	
	srand(unsigned(time(NULL)));
	int size = 0;
	cout << "Enter amount of elements in array: ";
	cin >> size;
	int *arr1 = new int[size];

	int addres = Fill_Arr(arr1, size);
	cout << "address of arr1 = " << &addres << endl;
	Print_Arr(arr1, size);
	cout << endl;

	cout << "Enter one number: ";
	int val = 0;
	cin >> val;
	Push_Back_El(arr1, size, val);
	Print_Arr(arr1, size);


	system("pause");
	return 0;
}