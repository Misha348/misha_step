#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

template <typename T1, typename T2, typename T3>
void Compare(T1 *pA, T2 *pB, T3 *pMAX )
{
	if (*pA > *pB)
	{
		*pMAX = *pA;
	}
	if(*pB > *pA)
	{
		*pMAX = *pB;
	}
}


int main()
{
	int max = 0;
	int *pMAX = &max;

	int a = 0;
	cout << "Enter 1 number: ";
	cin >> a;
	int *pA = &a;

	int b = 0;
	cout << "Enter 2 number: ";
	cin >> b;
	int *pB = &b;

	Compare(pA, pB, pMAX);
	cout << "Biger is: " << max << endl;

	system("pause");
	return 0;
}

