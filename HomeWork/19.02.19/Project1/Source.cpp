#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

template <typename T1, typename T2, typename T3>
void Replace(T1 *pA, T2 *pB, T3 *pC)
{
	cout << "\t\tStart positions of numbers\n" << endl;
	cout << "Value 1 = " << *pA << "\tAdress 1 = " << pA << endl;
	cout << "Value 2 = " << *pB << "\tAdress 2 = " << pB << endl;
	cout << "Value 3 = " << *pC << "\tAdress 3 = " << pC << endl;

	int temp = *pA;
	*pA = *pB;
	*pB = temp;

	cout << "\n\t\tafter first replace positions\n" << endl;
	cout << "Value 1 = " << *pA << "\tAdress 1 = " << pA << endl;
	cout << "Value 2 = " << *pB << "\tAdress 2 = " << pB << endl;
	cout << "Value 3 = " << *pC << "\tAdress 3 = " << pC << endl;

	int temp1 = *pB;
	*pB = *pC;
	*pC = temp;

	cout << "\n\t\tafter second replace positions\n" << endl;
	cout << "Value 1 = " << *pA << "\tAdress 1 = " << pA << endl;
	cout << "Value 2 = " << *pB << "\tAdress 2 = " << pB << endl;
	cout << "Value 3 = " << *pC << "\tAdress 3 = " << pC << endl;

}

int main()
{
	int a = 0;
	cout << "Enter 1 number:";
	cin >> a;
	int *pA = &a;

	int b = 0;
	cout << "Enter 2 number:";
	cin >> b;
	int *pB = &b;

	int c = 0;
	cout << "Enter 3 number:";
	cin >> c;
	cout << "\n";
	int *pC = &c;

	Replace(pA, pB, pC);


	system("pause");
	return 0;
}