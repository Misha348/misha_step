#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;


template < typename T1, typename T2, typename T3 >
void AddFunk(T1 *p1, T2 *p2, T3 *pADD)
{
	*pADD = (*p1) + (*p2);
	cout << "res = " << *pADD << endl;
}

template < typename T1, typename T2, typename T3 >
void SubsFunk(T1 *p1, T2 *p2, T3 *pSUBS)
{
	*pSUBS = (*p1) - (*p2);
	cout << "res = " << *pSUBS << endl;
}

template < typename T1, typename T2, typename T3 >
void MultFunk(T1 *p1, T2 *p2, T3 *pMULT)
{
	*pMULT = (*p1) * (*p2);
	cout << "res = " << *pMULT << endl;
}

template < typename T1, typename T2, typename T3 >
void DivFunk(T1 *p1, T2 *p2, T3 *pDIV)
{
	if (*p2 == 0)
	{
		cout << "Impossible to divide with 0 !!" << endl;
	}
	if (*p2 != 0)
	{
		*pDIV = (*p1) / (*p2);
		cout << "res = " << *pDIV << endl;
	}
}

int main()
{
	cout << "\twhat type of numbers to use:\n[ 1 ] - integer;\n[ 2 ] - fractal." << endl;
	int choise2 = 0;
	cin >> choise2;
	if (choise2 == 1)
	{
		int add = 0;
		int *pADD = &add;
		int subs = 0;
		int *pSUBS = &subs;
		int mult = 0;
		int *pMULT = &mult;
		double div = 0;
		double *pDIV = &div;

		cout << "\tChoose operation\n[ 1 ] - add;\n[ 2 ] - substruct;\n[ 3 ] - multiply;\n[ 4 ] divide;" << endl;
		int choise = 0;
		cin >> choise;

		int a = 0;
		cout << "Enter first number: ";
		cin >> a;
		int *p1 = &a;

		int b = 0;
		cout << "Enter second number: ";
		cin >> b;
		int *p2 = &b;

		if (choise == 1)
		{
			AddFunk(p1, p2, pADD);

		}
		if (choise == 2)
		{
			SubsFunk(p1, p2, pSUBS);
		}
		if (choise == 3)
		{
			MultFunk(p1, p2, pMULT);
		}
		if (choise == 4)
		{
			DivFunk(p1, p2, pDIV);
		}
	}
	if (choise2 == 2)
	{
		double add = 0;
		double *pADD = &add;
		double subs = 0;
		double *pSUBS = &subs;
		double mult = 0;
		double *pMULT = &mult;
		double div = 0;
		double *pDIV = &div;

		cout << "\tChoose operation\n[ 1 ] - add;\n[ 2 ] - substruct;\n[ 3 ] - multiply;\n[ 4 ] divide;" << endl;
		int choise = 0;
		cin >> choise;

		double a = 0;
		cout << "Enter first number: ";
		cin >> a;
		double *p1 = &a;

		double b = 0;
		cout << "Enter second number: ";
		cin >> b;
		double *p2 = &b;

		if (choise == 1)
		{
			AddFunk(p1, p2, pADD);

		}
		if (choise == 2)
		{
			SubsFunk(p1, p2, pSUBS);
		}
		if (choise == 3)
		{
			MultFunk(p1, p2, pMULT);
		}
		if (choise == 4)
		{
			DivFunk(p1, p2, pDIV);
		}

	}

	


	

	
	


	system("pause");
	return 0;
}