# include <iostream>


using namespace std;

void FVAR(int a) {
	cout << "FVAR BEFORE a  value: " << a << " : a addr : " << &a << endl;
	a += 100;
	cout << "FVAR AFTER a  value: " << a << " : a addr : " << &a << endl;
}
void FREF(int &a) {
	cout << "FREF BEFORE a  value: " << a << " : a addr : " << &a << endl;
	a += 300;
	cout << "FREF AFTER a  value: " << a << " : a addr : " << &a << endl;
}

void FPTR(int *a) {
	cout << "FPTR BEFORE a  value: " << *a << " : a addr : " << a << endl;
	*a += 500;
	cout << "FPTR AFTER a  value: " << *a << " : a addr : " << a << endl;
}


int main() {

	int a = 10;

	cout << "main a  value: " << a << "  : a addr : " << &a << endl;
	//int *pA = &a;
	//cout << "*pA  value: " << *pA << " : *pA addr : " << pA << endl;
	//cout << "========================Before Ra==================>" << endl;

	//int &Ra = a;

	//cout << "a  value: " << a << " : a addr : " << &a << endl;
	//cout << "&Ra  value: " << Ra << " : &Ra addr : " << &Ra << endl;
	cout << "========================After Ra==================>" << endl;

	FVAR(a);
	cout << "===================================================>" << endl;
	//FREF(Ra);
	FREF(a); //FREF( Ra );
	cout << "================================================>" << endl;
	FPTR(&a);
	cout << "================================================>" << endl;

	cout << "main a  value: " << a << "  : a addr : " << &a << endl;

	system("pause");
	return 0;
}