# include <iostream>;
# include <ctime>;
# include <windows.h>;

using namespace std;

template <typename T1, typename T2, typename T3>
void Multiply(T1 a, T2 b, T3 c)
{
	cout <<"int a = "<< *a << ", double b = " << *b << ", float c = " << *c << endl;
	cout << " Multiply of these varialables = " << (*a) * (*b) * (*c) << endl;	
}

int main()
{
	int *a = new int(23);
		
	double *b = new double(12.3);

	float *c = new float(0.8);
	Multiply(a, b, c);

	delete a;
	delete b;
	delete c;
	system("pause");
	return 0;
}