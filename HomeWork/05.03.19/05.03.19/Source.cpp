# include <iostream>;
# include <ctime>;
# include <windows.h>;
using namespace std;


void Fill_Arr(int arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = rand() % 20 + (-10);
	}
}

void Print_Arr(int arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "el [" << i << "] = " << arr[i] << endl;
	}
}

void Find_Min(int arr[], int size, int &min)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (arr[i] < arr[j])
			{
				min = arr[i];
			}
		}
	}
	cout << "VAL min = " << min << endl;
	cout << "Double min = " << min * 2 << endl;
	cout << " REF min = " << &min << endl;
}

int main()
{	
	srand(unsigned(time(NULL)));
	const int size = 5;
	int arr[size];

	Fill_Arr(arr, size);
	Print_Arr(arr, size);
	int min = 0;
	cout << "Adr min = " << &min << endl;

	Find_Min(arr, size, min);
	

	system("pause");
	return 0;
}

