# include <iostream>;
# include <ctime>;
# include <windows.h>;
using namespace std;

void Fill_Arr(int arr[], int SIZE)
{
	for (int i = 0; i < SIZE; i++)
	{
		arr[i] = rand() % 60 + (-30);
	}
}

void Print_Arr(int arr[], int SIZE)
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << "el. [" << i << "] = " << arr[i] << endl;
	}
}

void Change_Min_El(int arr[], int SIZE, int &negative)
{
	cout << "test negative value = " << negative << endl;
	cout << "test negative addr = " << &negative << endl;
	for (int i = 0; i < SIZE; i++)
	{
		if (arr[i] < 0)
		{
			negative = arr[i];		
			cout << negative << endl;
			cout << "REF Negative -- " << &negative << endl;
		}
	}

	for (int i = 0; i < SIZE; i++)
	{
		if (arr[i] < 0)
		{
			arr[i] = 0;
		}
		cout << "el. [" << i << "] = " << arr[i] << endl;
	}	
}

int main()
{
	srand(unsigned(time(NULL)));
	const int SIZE = 8;
	int arr[SIZE];
	//int *pArr = arr;

	Fill_Arr(arr, SIZE);
	Print_Arr(arr, SIZE);
	cout << "<--------------------------->" << endl;
	int negative = 0;
	Change_Min_El(arr, SIZE, &negative);

	system("pause");
	return 0;
}
