#include <iostream>
#include <ctime>;
#include <windows.h>
using namespace std;

int EnterNumber()
{
	int number = 0;	
	cin >> number;
	return number;
}
void ShowArray(int n, int choise)
{	
	int *arr = new int[n];
	for (int i = 0; i < n; i++)
	{
		cout << "Enter " << i + 1 << " element: ";
		cin >> arr[i];
	}
	cout << "\nYour array:" << endl;
	for(int i = 0; i < n; i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
	cout << "\nOrdered array:\n" << endl;

	if (choise == 1)
	{
		for (int i = 0; i < n - 1; i++)
		{
			for (int j = i + 1; j < n; j++)
			{
				if (arr[i] > arr[j])
				{
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		for (int i = 0; i < n; i++)
		{
			cout << arr[i] << " ";
		}
	}
	if (choise == 2)
	{
		for (int i = 0; i < n - 1; i++)
		{
			for (int j = i + 1; j < n; j++)
			{
				if (arr[i] < arr[j])
				{
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		for (int i = 0; i < n; i++)
		{
			cout << arr[i] << " ";
		}
	}
}

int main()
{	
	/*----------------------впорядкування масиву-----------------------------*/
	cout << "Enter the amount of arrays elements: " << endl;
	int n = EnterNumber();
	cout << "How to print array? [ 1 - from MIN to MAX ]\t[ 2 - from MAX to MIN ]" << endl;
	int choise = EnterNumber();
	ShowArray(n, choise);	

	system("pause");
	return 0;
}