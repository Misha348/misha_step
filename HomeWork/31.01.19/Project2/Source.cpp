#include <iostream>
#include <ctime>;
#include <windows.h>
using namespace std;


int EnterNumber()
{
	int number = 0;
	cin >> number;
	return number;
}

int ShowResults( int round)
{
	int max = 0;
	int min = 0;
	int sum = 0;
	int sum1 = 0;
	int *arr = new int[round];
	for (int i = 0; i < round; i++)
	{
		cout << "  " << i + 1 << " level: " << endl;
		cin >> arr[i];
	}
	for (int i = 0; i < round; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}
		if (arr[i] < min)
		{
			min = arr[i];
		}		
		sum += arr[i];
		sum1 = sum - min - max;
	}	
	return sum1;
}

// ���, ��������, ����
int main()
{
	int members = 0;
	int round = 0;
	int p = 0;
	int k = 1;
	float average = 0;
	float *averages = new float[members];
	
	cout << "\t\t-- Enter quantity of participants of the show: " << endl;
	members = EnterNumber();
	cout << "\t\t-- Enter quantitu of levels:" << endl;
	round = EnterNumber();

	while (k <= members)
	{
		int sum = 0;
		cout << "\n\t-- Points of [" << k << "] participant -- " << endl;
		sum = ShowResults(round);
		average = (float)sum / (float)round;
		cout << "\nAverage point of [" << k << "] participant:" << average << endl;
		averages[p] = average;
		p++;

		Sleep(2000);
		system("cls");		
		k++;
	}

	cout << "Points statistic:\n " << endl;
	for (int i = 0; i < members; i++)
	{
		cout << i + 1 << " participant  -- " << averages[i] << " points " << endl;
	}

	system("pause");
	return 0;
}