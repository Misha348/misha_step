#include <iostream>
#include <string>
using namespace std;
//�������� ��������, ��� ������ ��������� ����� � �������� ������������ ������� �������� � ���� ����������� ���������.
//����������� �������� ��������� (��� ������ �� ϲ�). 

struct AddInfo
{
	string email;
	string company;

	void ShowAddInfo()
	{
		cout << "Email: " << email << "\nCompany: " << company << endl;
	}
};

struct Abonent
{
	string name;
	string surname;
	string telnum;		

	void ShowAbInfo()
	{
		cout << "Name: " << name << "\nSurname: " << surname << "\nTel.numb: " << telnum << endl;
	}
	AddInfo addinfo;
};

void FillAbonentBook(int numOfAbon, Abonent* abonent)
{
	for (int i = 0; i < numOfAbon; i++)
	{
		cout << "Fill data of the " << i + 1 << " abonent:\n" << endl;
		cout << "Name: "; 
		cin >> abonent[i].name;
		cout << "Surname: ";
		cin >> abonent[i].surname;
		cout << "Tel.: ";
		cin >> abonent[i].telnum;
		cout << "Email: ";
		cin >> abonent[i].addinfo.email;
		cout << "Company: ";
		cin >> abonent[i].addinfo.company;
		cout << endl;	
		system("cls");
	}
}

void ShowAbonentBook(int numOfAbon, Abonent* abonent)
{
	for (int i = 0; i < numOfAbon; i++)
	{
		cout << "Abonent [" << i + 1 << "] " << endl;
		cout << "\nName: " << abonent[i].name << endl;
		cout << "Surname: " << abonent[i].surname << endl;
		cout << "Tel.: " << abonent[i].telnum << endl;
		cout << "Email: " << abonent[i].addinfo.email << endl;
		cout << "Company: " << abonent[i].addinfo.company << endl;		
		cout << "------------------" << endl;
	}
}

int main()
{
	cout << "\t\[ tAddress book ]\n\n" << endl;
	cout << "Enter number of abonents:" << endl;
	int numOfAbon = 0;
	cin >> numOfAbon;
	Abonent *abonent = new Abonent[numOfAbon];
	FillAbonentBook(numOfAbon, abonent);
	ShowAbonentBook(numOfAbon, abonent);	

	system("pause");
	return 0;
}