#include <iostream>
#include <string>

using namespace std;


struct Address {
	string country;
	string region;
	string city;
	string street;
	int zipCode;
	void ShowAddressInfo() {
		cout << "Country: " << country << "\nRegion: " << region << "\nCity: " << city << "\nStreet: " << street << "\nZipCode: " << zipCode << endl;
	}
};

struct Person {
private:
	string name;
	string surname;
	int age;
public:
	void ShowPersonInfo() {
		cout << "Name: " << name << "\nSurname: " << surname << "\nAge: " << age << endl;
	}
	void SetName(string newName) {
		name = newName;
	}
	void SetSurname(string newSurname) {
		surname = newSurname;
	}
	void SetAge(int newAge) {
		if (newAge >= 120 || newAge <= 0) {
			cout << "Incorrect age!" << endl;
		}
		else {
			age = newAge;
		}
	}
	int GetAge() {
		return age;
	}
	Address address;
};


int main() {

	Person Bill;
	//Bill.name = "Bill";
	//Bill.surname = "Gates";
	//Bill.age = 59;

	Bill.SetAge(90);
	//int billAge = Bill.GetAge();
	//cout << "Bill Age: " << billAge << endl;

	Bill.SetName("Bill");
	Bill.SetSurname("Gates");
	Bill.SetAge(60);
	Bill.ShowPersonInfo();
	Bill.address.country = "USA";
	Bill.address.region = "CA";
	Bill.address.city = "LA";
	Bill.address.street = "S. Banderu";
	Bill.address.zipCode = 43212;
	Bill.address.ShowAddressInfo();

	Person Stive;
	Stive.SetName("Stive");
	Stive.SetSurname("Balmar");
	Stive.SetAge(57);
	Stive.ShowPersonInfo();
	Stive.address.country = "USA";
	Stive.address.region = "CA";
	Stive.address.city = "SF";
	Stive.address.street = "R. Shuhevucha";
	Stive.address.zipCode = 345435;
	Stive.address.ShowAddressInfo();

	system("pause");
	return 0;
}