#include <iostream>;
#include <ctime>;
#include <string>;
using namespace std;

void PrintNewString(char *text, char *sbl, int pos)
{
	for (int i = 0; i < strlen(text); i++)
	{
		if (text[i] == sbl[0])
		{			
			pos = i;
		}
	}
	int size = (strlen(text) - 1);
	char newText[255];	
	
	for (int i = 0; i < pos; i++)
	{
		newText[i] = text[i];
	}
	for (int i = pos; i < (strlen(text) - 1 ); i++)
	{
		newText[i] = text[i + 1];
	}
	for (int i = 0; i < size; i++)
	{
		cout << newText[i] << " ";
	}
	cout << "\n";
}


int main()
{
	char text[255];
	cout << "Enter text: ";
	cin >> text;
	int pos = 0;
	
	char sbl[2];
	cout << "Enter symbol to delete: ";
	cin >> sbl;
	PrintNewString(text, sbl, pos);

	system("pause");
	return 0;
}