#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

int main()
{
	const int SIZE = 5;
	int arr[SIZE]{ 2, 8, 16, -4, -32 };
	for (int i = 0; i < SIZE; i++)
	{
		cout << "arr[" << i << "] = " << arr[i] << endl;
	}
	cout << "arrAddres = " << arr << endl; // �������� ��� 0 ��.
	cout << "-------------------------" << endl;

	int *pArr = arr;
	for (int i = 0; i < SIZE; i++)
	{
		cout << "arr[" << i << "] = " << pArr[i] << endl;
	}
	cout << "-------------------------" << endl;
	cout << "arrAddres = " << *pArr << endl;
	cout << "arr[1] addres = " << *(pArr + 1) << endl;
	cout << "arr[2] addres = " << *(pArr + 2) << endl;
	cout << "arr[2] addres = " << (pArr + 2) << endl;
	cout << "arr[3] addres = " << *(pArr + 3) << endl;
	cout << "-------------------------" << endl;

	for (int i = 0; i < SIZE; i++)
	{
		cout << "arr[" << i << "] = " << *(pArr + i) << endl;
	}

	system("pause");
	return 0;
}