#include <iostream>
#include <string>
using namespace std;

struct Person
{
	string name;
	int age;
};

void Sort(Person **q, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n - i - 1; j++)
		{
			if (q[j]->age < q[j + 1]->age)
			{
				Person *tmp = q[j];
				q[j] = q[j + 1];
				q[j + 1] = tmp;
			}
		}
	}
}

int main()
{
	int n = 0;
	cout << "num of persons: "; cin >> n;
	Person *p = new Person[n];
	for (int i = 0; i < n; i++)
	{
		cin >> p[i].name >> p[i].age;
	}
	cout << "----------" << endl;
		// creating of pointer for each el. in array(create array of pointers)
	Person **q = new Person*[n];
	for (int i = 0; i < n; i++)
	{
		q[i] = &p[i];
	}
	Sort(q, n);
	for (int i = 0; i < n; i++)
	{
		cout << q[i]->name << " " << q[i]->age << endl;
	}

	system("pause");
	return 0;
}
