#include <iostream>
#include <string>
using namespace std;

struct Person
{
	string name;
	string surname;
	short birthYear;
	short birthMonth;
	short birthDay;

	void ShowPersonsInfo()
	{
		cout << "\nName:\t\t" << name << "\nSurname:\t" << surname << "\nYear of birth:\t" << birthYear << "\nMonth of birth:\t" << birthMonth << "\nDay of birth:\t" << birthDay << endl;
	}
};

void Menu()
{
	cout << "----------------------------------" << endl;
	cout << "[ 1 ] - Show person's information.\n[ 2 ] - Show sorted list.\n[ 3 ] - Show birthday person.\n[ 4 ] - Add new person into list.\n[ 5 ] - Delete person from list.\n[ 6 ] - Find by name and surname.\n[ 7 ] - Change persons data.\n[ 0 ] - Exit." << endl;
	cout << "----------------------------------" << endl;
}

void FillPersonsList(Person *persons, int persNumb)
{
	cout << "\t\t[ Fill the list ]\n" << endl;
	for (int i = 0; i < persNumb; i++)
	{
		cout << "\nFill data for the " << i + 1 << " person:\n" << endl;
		cout << "Name: ";
		cin >> persons[i].name;
		cout << "Surname: ";
		cin >> persons[i].surname;
		cout << "Year of birth: ";
		cin >> persons[i].birthYear;
		cout << "Month of birth: ";
		cin >> persons[i].birthMonth;
		cout << "Day of birth: ";
		cin >> persons[i].birthDay;		
	}
}

void ShowPersonsList(Person *persons, int persNumb)		// 1
{
	for (int i = 0; i < persNumb; i++)
	{
		cout << "\n--------------------------" << endl;
		cout << "\nPerson [ " << i + 1 << " ]" << endl;
		persons[i].ShowPersonsInfo();		
	}
}

void SortedPersonsList(Person *&persons, int &persNumb)
{
	Person tmpPersons;
	for (int i = 0; i <= persNumb; i++)
	{
		for (int j = persNumb - 1; j > i; j--)
		{
			if (strcmp(persons[j].name.c_str(), persons[j - 1].name.c_str()) < 0)
			{
				tmpPersons = persons[j];
				persons[j] = persons[j - 1];
				persons[j - 1] = tmpPersons;
			}
		}
	}
}

void ShowBirthDayList(Person *persons, int persNumb)	// 3
{
	int month = 0;
	cout << "Enter month of birthdayers:"; cin >> month;
	for(int i = 0; i < persNumb; i++)
	{
		if (persons[i].birthMonth == month)
		{
			cout << persons[i].name << "\n" << persons[i].surname << "  --  " << persons[i].birthYear << "." << persons[i].birthMonth << "." << persons[i].birthDay << "\n" << endl;
		}
	}
}

void FindByNameOrSurname(Person *persons, int persNumb)	//6
{	
	string someEnteredName;
	string someEnteredSurname;
	cout << "Enter name and surname to find out a person in list:" << endl;
	cin >> someEnteredName;
	cin >> someEnteredSurname;
	int counter = 0;

	for (int i = 0; i < persNumb; i++)
	{
		if ((persons[i].name == someEnteredName) && (persons[i].surname == someEnteredSurname))
		{
			counter++;
			cout << persons[i].name << "\n" << persons[i].surname << "  --  " << persons[i].birthYear << "." << persons[i].birthMonth << "." << persons[i].birthDay << endl;
		}		
	}	
	if (counter == 0)
	{
		cout << "There's no such person in list." << endl;
	}
}

void AddOneMorePerson(Person *&persons, int &persNumb) // 4
{
	Person *tempPersons = new Person[persNumb + 1];
	for (int i = 0; i < persNumb; i++)
	{
		tempPersons[i] = persons[i];
	}
	cout << "Fill up data for new person: " << endl;
	cout << "Name: "; cin >> tempPersons[persNumb].name;
	cout << "Surname: "; cin >> tempPersons[persNumb].surname;
	cout << "Year of birth: "; cin >> tempPersons[persNumb].birthYear;
	cout << "Month of birth: "; cin >> tempPersons[persNumb].birthMonth;
	cout << "day of birth: "; cin >> tempPersons[persNumb].birthDay;
	persNumb++;

	delete[] persons;
	persons = tempPersons;	
}

void DeleteOnePerson(Person *&persons, int &persNumb) // 5
{
	int pos = 0;
	cout << "Enter possition in list to delete: "; 
	cin >> pos;
	int pos1 = pos - 1;
	Person *tempPersons = new Person[persNumb];
	for (int i = 0; i < persNumb; i++)
	{
		tempPersons[i] = persons[i];
	}
	persNumb--;
	for (int i = pos1; i < persNumb; i++)
	{
		tempPersons[i] = tempPersons[i + 1];
	}
	delete[] persons;
	persons = tempPersons;
}

void RerecordInfo(Person *persons, int persNumb)	// 7
{
	int pos = 0;
	cout << "Whos person's data to change:"; cin >> pos;
	for (int i = 0; i < persNumb; i++)
	{
		if (i == pos)
		{
			cout << "Name: "; cin >> persons[i].name;			
			cout << "Surname: "; cin >> persons[i].surname;
			cout << "Year of birth: "; cin >> persons[i].birthYear;
			cout << "Month of birth: "; cin >> persons[i].birthMonth;
			cout << "Day of birth: "; cin >> persons[i].birthDay;			
		}
	}
}

int main()
{
	int persNumb = 0;
	int choise = 0;
	cout << "\t\t[ LIST OF PERSONS ]\n" << endl;	
	
	enum action
	{
		ShowInformation = 1, // works
		ShowSortedList,
		BirthdayPerson,		 // works
		AddPerson,			 // works
		DeletePerson,		 // works
		FindByNameAndSurname,// works
		ChangePersonsInfo,	 // works
	};
	cout << "Enter number of persons in list: ";
	cin >> persNumb;

	Person *persons = new Person[persNumb];	
	FillPersonsList(persons, persNumb);	

	do
	{		
		Menu();
		cout << "\nMake choise for some action: ";
		cin >> choise;

		switch (choise)
		{
		case action::ShowInformation:
			ShowPersonsList(persons, persNumb); break;
		case action::ShowSortedList:
			SortedPersonsList(persons, persNumb);
			ShowPersonsList(persons, persNumb); break;

		case action::BirthdayPerson:
			ShowBirthDayList(persons, persNumb); break;		

		case action::AddPerson:
			AddOneMorePerson(persons, persNumb);
			ShowPersonsList(persons, persNumb); break;

		case action::DeletePerson:
			DeleteOnePerson(persons, persNumb);
			ShowPersonsList(persons, persNumb); break;

		case action::FindByNameAndSurname:
			FindByNameOrSurname(persons, persNumb); break;

		case action::ChangePersonsInfo:
			RerecordInfo(persons, persNumb);
			ShowPersonsList(persons, persNumb); break;
		}
		cin.get();
		cin.get();
		system("cls");
	} while (choise != 0);	
	
	delete[] persons;
	system("pause");
	return 0;
}