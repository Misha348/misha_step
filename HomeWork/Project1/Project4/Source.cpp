#include < iostream >
#include < windows.h >
#include < string >
#include < algorithm >

using namespace std;

string Give_Word(unsigned);

void Sort_Words(string *, unsigned, unsigned);

void Show_Words(string *, unsigned);

void main()
{
	setlocale(LC_ALL, "Russian");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	system("color 0D");

	cout << "������� ���������� ���� ";
	unsigned n;
	cin >> n;
	cin.get();

	auto a = new string[n];
	unsigned max_len = 0;

	for (unsigned u = 0; u < n; ++u)
	{
		auto str = Give_Word(u);

		if (str.size() > max_len)
		{
			max_len = str.size();
		}
		a[u] = str;
	}

	Sort_Words(a, n, max_len);

	Show_Words(a, n);

	system("pause");
}

string Give_Word(unsigned u)
{
	string str;
	for (;;)
	{
		cout << "������� ����� " << u + 1 << endl;
		getline(cin, str);
		if (str.size() == 0)
		{
			cout << "������ �����. ����������� ������ ������. ��������� ��� �����" << endl;
			str.clear();
			continue;
		}
		bool valid = true;
		for (auto c : str)
		{
			if (c == ' ')
			{
				valid = false;
				break;
			}
		}
		if (valid)
		{
			break;
		}
		else
		{
			cout << "������ �����. ����������� �������. ��������� ��� �����" << endl;
			str.clear();
		}
	}
	return str;
}

void Sort_Words(string *a, unsigned n, unsigned max_len)
{
	for (long long u = max_len - 1; u >= 0; --u)
	{
		auto p = [u](string s1, string s2)
		{
			unsigned s1_len = s1.size();
			unsigned s2_len = s2.size();
			unsigned short_ = (s1_len < s2_len) ? s1_len : s2_len;

			if (u <= short_ - 1)
			{
				return toupper((unsigned char)s1[u]) < toupper((unsigned char)s2[u]);
			}
			else
			{
				return s1_len < s2_len;
			}
		};
		sort(a, a + n, p);
	}
}

void Show_Words(string *a, unsigned n)
{
	
	cout << endl;
	for (unsigned u = 0; u < n; ++u)
	{
		cout << a[u] << endl;
	}
	cout << endl;
}